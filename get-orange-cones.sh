#!/bin/bash

COPY_DIR="./have_orange_cones"

if [ ! -d "$COPY_DIR" ]; then
  mkdir $COPY_DIR;
fi

if [ ! -d "$COPY_DIR/ann" ]; then
  mkdir "$COPY_DIR/ann";
fi

if [ ! -d "$COPY_DIR/img" ]; then
  mkdir "$COPY_DIR/img";
fi

if [ ! -d "$COPY_DIR/annotations" ]; then
  mkdir "$COPY_DIR/annotations";
fi

if [ "$(which jq)" == ""]; then
  echo "FATAL ERROR: jq not installed";
  exit 126;
fi


for f in $(find . -name \*.json)
do
  output=$(cat "$f" | grep "large_orange_cone\|orange_cone")
  if [[ $output != "" ]]
  then
    cp $f "$COPY_DIR/ann"
    echo $f
    f_img=${f%.*}
    f_img=$(echo $f_img | sed 's/ann/img/')
    cp $f_img "$COPY_DIR/img"
    echo $f_img
    f_orange_cones="$COPY_DIR/annotations/$(basename $f)"
    cat $f | jq 'del(.objects[] | select(.classTitle != "large_orange_cone" and .classTitle != "orange_cone"))' > $f_orange_cones
    echo $f_orange_cones
  fi
done
