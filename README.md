# get orange cones

`get-orange-cones.sh` contained within this repo is designed to extract the bounding boxes and images from the [FSOCO Dataset](https://www.fsoco-dataset.com/), making a subset that contains only `orange_cone` and `large_orange_cone` labels

We then use [`supervisely_json_to_yolo_txt.py`](https://gitlab.com/KSU_EVT/autonomous-software/supervisely_json_to_yolo_txt) to convert the bounding-box labels from superviesly `json` to yolo `txt` format

We combine this subset of the FSOCO dataset with our [own custom-labeled dataset](https://gitlab.com/KSU_EVT/autonomous-software/obtain_training_images) in the hopes to provide enough breadth and variety of training images to build a robust cone detection model.

We are expirememting using [Yolov7](https://github.com/WongKinYiu/yolov7), though a prototype using [yolov4/darknet](https://github.com/AlexeyAB/darknet) has proven succesful for inference@24fps running on the Oak-D Pro.